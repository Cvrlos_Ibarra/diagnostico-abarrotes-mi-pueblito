
create database abarrotespueblito;
use abarrotespueblito;


create table categoria(
idcategoria int primary key auto_increment,
categ varchar(50));

create table producto(
idproducto int primary key auto_increment,
nombreproducto varchar(50),
precio double,
fkcategoria int,
foreign key(fkcategoria) references categoria(idcategoria));

create table proveedor(
idproveedor int primary key auto_increment,
nombre varchar(50),
direccion varchar(100),
telefono int);

create table suministro(
idsuministro int primary key auto_increment,
fkproveedor int,
fkproducto int,
cantidad int,
fecha date,
foreign key(fkproducto) references producto(idproducto),
foreign key(fkproveedor) references proveedor(idproveedor));


insert into proveedor values(1,'Alfonso Gonzalez','Margaritas #78',4758526452);
insert into proveedor values(null,'Juana Maga�a','Arboles de la Barranca #12',4658527415);
insert into proveedor values(null,'Margarito Juarez','Lope de Vega #69',5742358549);

insert into categoria values(1,'refrescos'),(null,'frituras'),(null,'galletas'),(null,'Productos de limpieza'),(null,'frutas'),(null,'verduras');

insert into producto values(1,'coca-cola',13,1),(null,'cheetos',8,2),(null,'canelitas',11,3),(null,'pinol',15,4),(null,'manzana',7,5),(null,'zanahoria',4,6);

insert into suministro values(1,1,1,50,"13-02-2020");
insert into suministro values(null,1,2,25,"13-02-2020");
insert into suministro values(null,2,3,50,'2020-02-13');


#region vista
create view v_suministros as
select nombre as 'Nombre', fecha,categ as 'Categoria',nombreproducto as 'producto',precio,cantidad from proveedor,categoria,producto, suministro where fkproducto = producto.idproducto and fkproveedor = proveedor.idproveedor and fkcategoria = categoria.idcategoria;

#end region

#region Consultas


select nombreproducto, precio, categ as 'categoria' from producto, categoria where fkcategoria = idcategoria order by categ;
#end region



#region Procedure

create procedure InsertarSurtido2(in ids int, in idp int, in idpro int, in cant int, in fech date)
begin
if cant is null then
select "La cantidad no puede ser nula";
else
insert into suministro values(ids,idp,idpro,cant,fech);
select "Registro insertado";
end if;
end;

call InsertarSurtido2(null,1,1,1,'2020-02-18');

#end region


