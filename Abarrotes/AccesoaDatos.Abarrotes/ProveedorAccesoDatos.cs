﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.Abarrotes;

namespace AccesoaDatos.Abarrotes
{
    public class ProveedorAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ProveedorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "abarrotespueblito", 3306);
        }
        public void Guardar(Proveedor proveedor)
        {
            if (proveedor.Idproveedor == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into proveedor values(null,'{0}','{1}','{2}') ",
                    proveedor.Nombre, proveedor.Direccion, proveedor.Telefono);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update proveedor set nombre = '{0}',direccion = '{1}',telefono = '{2}' where idproveedor = {3}", proveedor.Nombre, proveedor.Direccion, proveedor.Telefono,proveedor.Idproveedor);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idproveedor)
        {
            //eliminar
            string consulta = string.Format("Delete from proveedor where idproveedor = '{0}'", idproveedor);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Proveedor> GetProveedor(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listproveedor = new List<Proveedor>();
            var ds = new DataSet();
            string consulta = "Select * from proveedor where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "proveedor");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var proveedor = new Proveedor
                {
                    Idproveedor = Convert.ToInt32(row["idproveedor"]),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = Convert.ToInt32(row["telefono"])


                };
                listproveedor.Add(proveedor);
            }
            return listproveedor;
        }
    }
}
