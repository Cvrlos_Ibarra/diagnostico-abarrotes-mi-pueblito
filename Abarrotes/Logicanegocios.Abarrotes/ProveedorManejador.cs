﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Abarrotes;
using AccesoaDatos.Abarrotes;

namespace Logicanegocios.Abarrotes
{
    public class ProveedorManejador
    {

        private ProveedorAccesoDatos _proveedoraccesodatos = new ProveedorAccesoDatos();
        public void Guardar(Proveedor proveedor)
        {
            _proveedoraccesodatos.Guardar(proveedor);

        }
        public void Eliminar(int idproveedor)
        {
            //eliminar
            _proveedoraccesodatos.Eliminar(idproveedor);
        }
        public List<Proveedor> GetProveedor(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listProveedor = _proveedoraccesodatos.GetProveedor(filtro);
            //Llenar lista
            return listProveedor;
        }
    }
}
